/**
 * aries-web-backend
 * Copyright (C)   Colibri-Embedded
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @file user.cpp
 *
 * @author Daniel Kesler <kesler.daniel@gmail.com>
 *
 */
#include "database/models/user.hpp"

// section:manual_includes
// endsection:manual_includes

namespace aries {
namespace database {

    void User::setId(int value) { m_id = value; }

    int User::getId() const { return m_id; }

    void User::setName(const std::string& value) { m_name = value; }

    std::string User::getName() const { return m_name; }

    void User::setEmail(const std::string& value) { m_email = value; }

    std::string User::getEmail() const { return m_email; }

    void User::setPassword(const std::string& value) { m_password = value; }

    std::string User::getPassword() const { return m_password; }

    void User::setAvatarUrl(const std::string& value) { m_avatar_url = value; }

    std::string User::getAvatarUrl() const { return m_avatar_url; }

    void User::setRole(unsigned value) { m_role = value; }

    unsigned User::getRole() const { return m_role; }

} // namespace database

} // namespace aries

// section:manual_code
// endsection:manual_code