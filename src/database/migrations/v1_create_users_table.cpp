/**
 * aries-web-backend
 * Copyright (C)   Colibri-Embedded
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @file v1_create_users_table.cpp
 *
 * @author Daniel Kesler <kesler.daniel@gmail.com>
 *
 */
#include "database/migrations/v1_create_users_table.hpp"

// section:manual_includes
#include "database/schema.hpp"
// endsection:manual_includes

namespace aries {
namespace database {

    void V1CreateUsersTable::up(SchemaInterface& schema)
    {
        // section:up_gelvcoe2npkrc7szdxq5ly523a
        schema.createIfNotExists("users", [](aries::database::Blueprint& table) {
            table.increments("id");
            table.string("name");
            table.string("email").unique();
            table.string("password");
            table.string("avatar_url").nullable();
            table._enum("role", { "BACKEND", "ADMIN", "ENGINEER", "PILOT" })._default("PILOT");
            table.timestamps();

            table.integer("company_id");
            table.foreign("company_id").references("id").on("companies").onDelete(ForeignKeyAction::CASCADE);
        });
        // endsection:up_gelvcoe2npkrc7szdxq5ly523a
    }

    void V1CreateUsersTable::down(SchemaInterface& schema)
    {
        // section:down_clyc6dy2fhyp5wmqc5vnumb2fe
        schema.dropIfExists("users");
        // endsection:down_clyc6dy2fhyp5wmqc5vnumb2fe
    }

} // namespace database

} // namespace aries

// section:manual_code
// endsection:manual_code