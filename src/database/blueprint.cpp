/**
 * aries-web-backend
 * Copyright (C)   Colibri-Embedded
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @file blueprint.cpp
 *
 * @author Daniel Kesler <kesler.daniel@gmail.com>
 *
 */
#include "database/blueprint.hpp"

// section:manual_includes
// endsection:manual_includes

namespace aries {
namespace database {

    ColumnDefinition& Blueprint::increments(const std::string& column)
    {
        // section:increments_eslxeyl56xl4g7lewygooahwli
        m_columns.emplace_back(column, "INTEGER");
        return m_columns.back().primary().autoIncrement();
        // endsection:increments_eslxeyl56xl4g7lewygooahwli
    }

    ColumnDefinition& Blueprint::integer(const std::string& column)
    {
        // section:integer_ax53kmovmsm2lehcwtnmr6ngfy
        m_columns.emplace_back(column, "INTEGER");
        return m_columns.back();
        // endsection:integer_ax53kmovmsm2lehcwtnmr6ngfy
    }

    ColumnDefinition& Blueprint::_float(const std::string& column, unsigned digits, unsigned decimals)
    {
        // section:float_56qnqcvsflr5zw7xarjrkymruu
        m_columns.emplace_back(column, "FLOAT", std::vector<unsigned>{ digits, decimals });
        return m_columns.back();
        // endsection:float_56qnqcvsflr5zw7xarjrkymruu
    }

    ColumnDefinition& Blueprint::_double(const std::string& column, unsigned digits, unsigned decimals)
    {
        // section:double_ajvgse5lhhqiqjqx24vsohrvzq
        m_columns.emplace_back(column, "DOUBLE", std::vector<unsigned>{ digits, decimals });
        return m_columns.back();
        // endsection:double_ajvgse5lhhqiqjqx24vsohrvzq
    }

    ColumnDefinition& Blueprint::decimal(const std::string& column, unsigned digits, unsigned decimals)
    {
        // section:decimal_xbidk7gqj2ss5r4c73lfuhei7q
        m_columns.emplace_back(column, "DECIMAL", std::vector<unsigned>{ digits, decimals });
        return m_columns.back();
        // endsection:decimal_xbidk7gqj2ss5r4c73lfuhei7q
    }

    ColumnDefinition& Blueprint::boolean(const std::string& column)
    {
        // section:boolean_wtokryif2hlj4rx2guq3ppkj2q
        m_columns.emplace_back(column, "BOOLEAN");
        return m_columns.back();
        // endsection:boolean_wtokryif2hlj4rx2guq3ppkj2q
    }

    ColumnDefinition& Blueprint::string(const std::string& column, unsigned length)
    {
        // section:string_6vnx3oadlmz2dlktou44i225ie
        m_columns.emplace_back(column, "VARCHAR", std::vector<unsigned>{ length });
        return m_columns.back();
        // endsection:string_6vnx3oadlmz2dlktou44i225ie
    }

    ColumnDefinition& Blueprint::text(const std::string& column)
    {
        // section:text_hbvxrg2xxf7uzcpfbvfusfq3gu
        m_columns.emplace_back(column, "TEXT");
        return m_columns.back();
        // endsection:text_hbvxrg2xxf7uzcpfbvfusfq3gu
    }

    ColumnDefinition& Blueprint::_enum(const std::string& column, const std::vector<std::string>& values)
    {
        // section:_enum_q5chtfsqtfkd2zckp2mzygcpty
        m_columns.emplace_back(column, "VARCHAR");
        auto& ref = m_columns.back();
        ref.check(CheckAction::IN_VALUES, values);
        return ref;
        // endsection:_enum_q5chtfsqtfkd2zckp2mzygcpty
    }

    ColumnDefinition& Blueprint::timestamp(const std::string& column)
    {
        // section:timestamp_fcadaumqoljpqrn5336mv4sfiu
        m_columns.emplace_back(column, "DATETIME");
        return m_columns.back();
        // endsection:timestamp_fcadaumqoljpqrn5336mv4sfiu
    }

    void Blueprint::timestamps()
    {
        // section:timestamps_obpmqlxzn5t2jzmltmgv5uhf54
        m_columns.emplace_back("created_at", "DATETIME");
        m_columns.back().nullable();
        m_columns.emplace_back("updated_at", "DATETIME");
        m_columns.back().nullable();
        // endsection:timestamps_obpmqlxzn5t2jzmltmgv5uhf54
    }

    ColumnDefinition& Blueprint::foreign(const std::string& column)
    {
        // section:foreign_bq5qk5bndp6oqzpmg6sojb53ku
        m_columns.emplace_back(column, "FOREIGN");
        return m_columns.back();
        // endsection:foreign_bq5qk5bndp6oqzpmg6sojb53ku
    }

    ColumnDefinition& Blueprint::foreignId(const std::string& column)
    {
        // section:foreignId_qypwagteenpytmictrqy74xdoi
        integer(column)._unsigned();
        m_columns.emplace_back(column, "FOREIGN");
        return m_columns.back();
        // endsection:foreignId_qypwagteenpytmictrqy74xdoi
    }

    const std::vector<ColumnDefinition>& Blueprint::getColumns() const { return m_columns; }

} // namespace database

} // namespace aries

// section:manual_code
// endsection:manual_code