/**
 * aries-web-backend
 * Copyright (C)   Colibri-Embedded
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @file database.cpp
 *
 * @author Daniel Kesler <kesler.daniel@gmail.com>
 *
 */
#include "database/database.hpp"

// section:manual_includes
// endsection:manual_includes

namespace aries {
namespace database {

    bool Database::open(const std::string& filename)
    {
        // section:open_gbeonif6bsmlbqlgc7z4o6dheq
        /* add your implementation here */
        // endsection:open_gbeonif6bsmlbqlgc7z4o6dheq
    }

    void Database::close()
    {
        // section:close_ofxwwmczrorqsroyisc6mhaqe4
        /* add your implementation here */
        // endsection:close_ofxwwmczrorqsroyisc6mhaqe4
    }

} // namespace database

} // namespace aries

// section:manual_code
// endsection:manual_code