/**
 * aries-web-backend
 * Copyright (C)   Colibri-Embedded
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @file column_definition.cpp
 *
 * @author Daniel Kesler <kesler.daniel@gmail.com>
 *
 */
#include "database/column_definition.hpp"

// section:manual_includes
// endsection:manual_includes

namespace aries {
namespace database {

    ColumnDefinition::ColumnDefinition(const std::string& name,
                                       const std::string& _type,
                                       const std::vector<unsigned>& size)

    {
        // section:<create>_iujbrx3uu4vlvdszzwyctjs6mq
        m_name    = name;
        m_type    = _type;
        m_size    = size;
        m_foreign = (_type == "FOREIGN");
        // endsection:<create>_iujbrx3uu4vlvdszzwyctjs6mq
    }

    ColumnDefinition& ColumnDefinition::_default(const std::string& value)
    {
        // section:default_euw4kvsmfertymfwg7nchk4uvy
        m_default       = true;
        m_default_value = "'" + value + "'";
        return *this;
        // endsection:default_euw4kvsmfertymfwg7nchk4uvy
    }

    ColumnDefinition& ColumnDefinition::_default(int value)
    {
        // section:default_pbhqoc67if6jtnhb45hbhjtcbq
        m_default       = true;
        m_default_value = std::to_string(value);
        return *this;
        // endsection:default_pbhqoc67if6jtnhb45hbhjtcbq
    }

    ColumnDefinition& ColumnDefinition::_default(double value)
    {
        // section:default_mr55hma6gqh6annydpmsfq4o6i
        m_default       = true;
        m_default_value = std::to_string(value);
        return *this;
        // endsection:default_mr55hma6gqh6annydpmsfq4o6i
    }

    ColumnDefinition& ColumnDefinition::_unsigned(bool value)
    {
        // section:unsigned_dcdbw5z5ewvriqt4aejxpn3fqm
        /* add your implementation here */
        return *this;
        // endsection:unsigned_dcdbw5z5ewvriqt4aejxpn3fqm
    }

    ColumnDefinition& ColumnDefinition::nullable(bool value)
    {
        // section:nullable_ctffavnurjocyl3xg4yzcs5rdm
        m_nullable = value;
        return *this;
        // endsection:nullable_ctffavnurjocyl3xg4yzcs5rdm
    }

    ColumnDefinition& ColumnDefinition::unique(bool value)
    {
        // section:unique_uhekrmsct3yhjkgncq2zobb5gm
        m_unique = value;
        return *this;
        // endsection:unique_uhekrmsct3yhjkgncq2zobb5gm
    }

    ColumnDefinition& ColumnDefinition::index(bool value)
    {
        // section:index_254h2hw4yp2vsjtclws4yo6v2a
        m_index = value;
        return *this;
        // endsection:index_254h2hw4yp2vsjtclws4yo6v2a
    }

    ColumnDefinition& ColumnDefinition::primary(bool value)
    {
        // section:primary_agtwsiodzdfhznfy4vgmordxsa
        m_primary = value;
        return *this;
        // endsection:primary_agtwsiodzdfhznfy4vgmordxsa
    }

    ColumnDefinition& ColumnDefinition::autoIncrement(bool value)
    {
        // section:auto_increment_mlcafskwpffmjdpvt2anbyg24a
        m_autoincrement = value;
        return *this;
        // endsection:auto_increment_mlcafskwpffmjdpvt2anbyg24a
    }

    ColumnDefinition& ColumnDefinition::on(const std::string& table)
    {
        // section:on_3xnlsuf4jgqyzpjwwu3tabg4ly
        m_foreign       = true;
        m_foreign_table = table;
        return *this;
        // endsection:on_3xnlsuf4jgqyzpjwwu3tabg4ly
    }

    ColumnDefinition& ColumnDefinition::onDelete(ForeignKeyAction action)
    {
        // section:on_delete_kbigddmuglggvwkpe6cxpdbr7u
        std::string action_str;
        switch (action) {
            case ForeignKeyAction::NO_ACTION:
                action_str = "NO ACTION";
                break;
            case ForeignKeyAction::RESTRICT:
                action_str = "RESTRICT";
                break;
            case ForeignKeyAction::SET_NULL:
                action_str = "SET NULL";
                break;
            case ForeignKeyAction::SET_DEFAULT:
                action_str = "SET DEFAULT";
                break;
            case ForeignKeyAction::CASCADE:
                action_str = "CASCADE";
                break;
        }
        m_on_delete = action_str;
        return *this;
        // endsection:on_delete_kbigddmuglggvwkpe6cxpdbr7u
    }

    ColumnDefinition& ColumnDefinition::onUpdate(ForeignKeyAction action)
    {
        // section:on_update_kiikqxvztkiza7bcjgaftspnba
        std::string action_str;
        switch (action) {
            case ForeignKeyAction::NO_ACTION:
                action_str = "NO ACTION";
                break;
            case ForeignKeyAction::RESTRICT:
                action_str = "RESTRICT";
                break;
            case ForeignKeyAction::SET_NULL:
                action_str = "SET NULL";
                break;
            case ForeignKeyAction::SET_DEFAULT:
                action_str = "SET DEFAULT";
                break;
            case ForeignKeyAction::CASCADE:
                action_str = "CASCADE";
                break;
        }
        m_on_update = action_str;
        return *this;
        // endsection:on_update_kiikqxvztkiza7bcjgaftspnba
    }

    ColumnDefinition& ColumnDefinition::references(const std::string& column)
    {
        // section:references_brcsbpfecaecwbodc7egfmjsbu
        m_foreign            = true;
        m_foreign_references = column;
        return *this;
        // endsection:references_brcsbpfecaecwbodc7egfmjsbu
    }

    ColumnDefinition& ColumnDefinition::check(CheckAction action, const std::vector<std::string>& values)
    {
        // section:check_3q7xpe3kyps3da6tdttmdmsuzm
        m_checks.push_back(CheckExpression{ action, 0, values });
        return *this;
        // endsection:check_3q7xpe3kyps3da6tdttmdmsuzm
    }

    ColumnDefinition& ColumnDefinition::check(CheckAction action, int value)
    {
        // section:check_afdjspqsgh44gn2aekiigk6hdy
        m_checks.push_back(CheckExpression{ action, value });
        return *this;
        // endsection:check_afdjspqsgh44gn2aekiigk6hdy
    }

    std::string ColumnDefinition::getName() const { return m_name; }

    std::string ColumnDefinition::getType() const { return m_type; }

    const std::vector<unsigned>& ColumnDefinition::getSize() const { return m_size; }

    bool ColumnDefinition::getIndex() const { return m_index; }

    bool ColumnDefinition::getAutoincrement() const { return m_autoincrement; }

    bool ColumnDefinition::getNullable() const { return m_nullable; }

    bool ColumnDefinition::getPrimary() const { return m_primary; }

    bool ColumnDefinition::getUnique() const { return m_unique; }

    bool ColumnDefinition::getDefault() const { return m_default; }

    std::string ColumnDefinition::getDefaultValue() const { return m_default_value; }

    bool ColumnDefinition::getForeign() const { return m_foreign; }

    std::string ColumnDefinition::getForeignTable() const { return m_foreign_table; }

    std::string ColumnDefinition::getForeignReferences() const { return m_foreign_references; }

    std::string ColumnDefinition::getOnDelete() const { return m_on_delete; }

    std::string ColumnDefinition::getOnUpdate() const { return m_on_update; }

    const std::vector<CheckExpression>& ColumnDefinition::getChecks() const { return m_checks; }

} // namespace database

} // namespace aries

// section:manual_code
// endsection:manual_code