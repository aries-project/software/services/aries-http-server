/**
 * aries-web-backend
 * Copyright (C)   Colibri-Embedded
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @file schema.cpp
 *
 * @author Daniel Kesler <kesler.daniel@gmail.com>
 *
 */
#include "database/schema.hpp"

// section:manual_includes
#include <iostream>
// endsection:manual_includes

namespace aries {
namespace database {

    Schema::Schema(Database& database)
      : m_database(database)
    {
        // section:<create>_ggoamwdpxmd355kodthbry7lnu
        /* add your implementation here */
        // endsection:<create>_ggoamwdpxmd355kodthbry7lnu
    }

    bool Schema::hasTable(const std::string& table)
    {
        // section:has_table_fvl7xudtnyabddu4zdr6utk6sm
        /* add your implementation here */
        // endsection:has_table_fvl7xudtnyabddu4zdr6utk6sm
    }

    bool Schema::create(const std::string& table, const CreateTableCallback& callback)
    {
        // section:create_6uaeipzfjmngywzprxe6jtr5te
        /* add your implementation here */
        // endsection:create_6uaeipzfjmngywzprxe6jtr5te
    }

    bool Schema::createIfNotExists(const std::string& table, const CreateTableCallback& callback)
    {
        // section:create_if_not_exists_guhfa4jl6m2my7bqolseekiziq
        Blueprint bp;
        callback(bp);
        std::cout << "CREATE TABLE IF NOT EXISTS \"" << table << "\" (" << std::endl;
        for (auto& col : bp.getColumns()) {
            auto stmt = columnDefinitionToStatement(col);
            std::cout << "   " << stmt << "," << std::endl;
        }
        std::cout << ")" << std::endl;
        return true;
        // endsection:create_if_not_exists_guhfa4jl6m2my7bqolseekiziq
    }

    bool Schema::drop(const std::string& table)
    {
        // section:drop_v62gvc2l2nz55nlqdzizqd6qsq
        /* add your implementation here */
        // endsection:drop_v62gvc2l2nz55nlqdzizqd6qsq
    }

    bool Schema::dropIfExists(const std::string& table)
    {
        // section:drop_if_exists_xo2qzpwixg45ewojd44jfaq2fm
        /* add your implementation here */
        // endsection:drop_if_exists_xo2qzpwixg45ewojd44jfaq2fm
    }

    bool Schema::dropColumn(const std::string& column)
    {
        // section:drop_column_rnuiy2ppxxfsunt37vu6xz4sm4
        /* add your implementation here */
        // endsection:drop_column_rnuiy2ppxxfsunt37vu6xz4sm4
    }

    bool Schema::renameColumn(const std::string& column)
    {
        // section:rename_column_lecr7yg6kcru4l7ycsp5wnnrjm
        /* add your implementation here */
        // endsection:rename_column_lecr7yg6kcru4l7ycsp5wnnrjm
    }

    bool Schema::dropPrimary(const std::string& column)
    {
        // section:drop_primary_riobpolgt7rvjydnffcugaj7bq
        /* add your implementation here */
        // endsection:drop_primary_riobpolgt7rvjydnffcugaj7bq
    }

    bool Schema::dropUnique(const std::string& column)
    {
        // section:drop_unique_rhc5jsbkmen6x75xiq6axaxjle
        /* add your implementation here */
        // endsection:drop_unique_rhc5jsbkmen6x75xiq6axaxjle
    }

    bool Schema::dropIndex(const std::string& column)
    {
        // section:drop_index_jikvuqoj4anf6mmsroj34o2qqm
        /* add your implementation here */
        // endsection:drop_index_jikvuqoj4anf6mmsroj34o2qqm
    }

    bool Schema::dropForeign(const std::string& column)
    {
        // section:drop_foreign_2pdbxuloyph7y3phflsavxn25a
        /* add your implementation here */
        // endsection:drop_foreign_2pdbxuloyph7y3phflsavxn25a
    }

    std::string Schema::columnDefinitionToStatement(const ColumnDefinition& def)
    {
        // section:column_definition_to_statement_nbkwqphcsmmzzf3hi5memis6du
        std::string statment;

        if (!def.getForeign()) {
            statment = "\"" + def.getName() + "\" " + def.getType();
            if (!def.getNullable()) {
                statment += " NOT NULL";
            }
            if (def.getDefault()) {
                statment += " DEFAULT " + def.getDefaultValue();
            }
            if (def.getPrimary()) {
                statment += " PRIMARY KEY";
            }
            if (def.getAutoincrement()) {
                statment += " AUTOINCREMENT";
            }
        } else {
            statment = "FOREIGN KEY(\"" + def.getName() + "\")";

            if (!def.getForeignTable().empty() && !def.getForeignReferences().empty()) {
                statment += " REFERENCES \"" + def.getForeignTable() + "\"";
                statment += "(\"" + def.getForeignReferences() + "\")";
            } else {
                // TODO: error handling
            }

            if (!def.getOnDelete().empty()) {
                statment += " ON DELETE " + def.getOnDelete();
            }

            if (!def.getOnUpdate().empty()) {
                statment += " ON DELETE " + def.getOnUpdate();
            }
        }

        auto& checks = def.getChecks();
        if (checks.size() > 0) {
            //
            for (auto& check : checks) {
                if (check.action == CheckAction::IN_VALUES) {
                    std::string expr;

                    expr += "\"" + def.getName() + "\" IN (";
                    bool comma = false;
                    for (auto& v : check.values) {
                        if (comma) {
                            expr += ", ";
                        }
                        expr += "'" + v + "'";
                        if (!comma) {
                            comma = true;
                        }
                    }
                    expr += ")";

                    statment += " CHECK(" + expr + ")";
                }
            }
        }

        return statment;
        // endsection:column_definition_to_statement_nbkwqphcsmmzzf3hi5memis6du
    }

    void Schema::setDatabase(const Database& value) { m_database = value; }

    Database Schema::getDatabase() const { return m_database; }

} // namespace database

} // namespace aries

// section:manual_code
// endsection:manual_code