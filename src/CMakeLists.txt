set(APP_NAME aries-http-server)

set(SOURCES
	"main.cpp"
	"http_server.cpp"

	"database/blueprint.cpp"
	"database/column_definition.cpp"
	"database/database.cpp"
	"database/schema.cpp"

	"database/models/user.cpp"

	"database/migrations/v1_create_users_table.cpp"
)

add_executable(${APP_NAME} ${SOURCES})
set_property(TARGET ${APP_NAME} PROPERTY CXX_STANDARD 14)
set_property(TARGET ${APP_NAME} PROPERTY CXX_STANDARD_REQUIRED ON)

target_include_directories(${APP_NAME} PUBLIC ${3RDPARTY_SOURCE_DIR})

add_definitions(${WAMPCC_CFLAGS} ${WAMPCC_CFLAGS_OTHER})
target_link_libraries (${APP_NAME} ${WAMPCC_LIBRARIES})

##
## Install targets
##
install (TARGETS ${APP_NAME} DESTINATION "${INSTALL_BIN_DIR}")

# add_subdirectory(plugins)