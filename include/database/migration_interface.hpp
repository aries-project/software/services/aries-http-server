/**
 * aries-web-backend
 * Copyright (C)   Colibri-Embedded
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @file migration_interface.hpp
 *
 * @author Daniel Kesler <kesler.daniel@gmail.com>
 *
 */
#ifndef MIGRATION_INTERFACE_HPP
#define MIGRATION_INTERFACE_HPP

#include "database/schema_interface.hpp"
// section:manual_includes
// endsection:manual_includes

namespace aries {

namespace database {

    /**
     *
     */
    class MigrationInterface
    {
        // section:manual_definitions
        // endsection:manual_definitions

      public:
        /**
         * Run the migration.
         * @param[out] schema
         */
        virtual void up(SchemaInterface& schema) = 0;

        /**
         * Reverse the migration.
         * @param[out] schema
         */
        virtual void down(SchemaInterface& schema) = 0;

      protected:
      private:
    };

} // namespace database

} // namespace aries

#endif /* MIGRATION_INTERFACE_HPP */