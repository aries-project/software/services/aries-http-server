/**
 * aries-web-backend
 * Copyright (C)   Colibri-Embedded
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @file database_interface.hpp
 *
 * @author Daniel Kesler <kesler.daniel@gmail.com>
 *
 */
#ifndef DATABASE_INTERFACE_HPP
#define DATABASE_INTERFACE_HPP

#include <string>
// section:manual_includes
// endsection:manual_includes

namespace aries {

namespace database {

    /**
     *
     */
    class DatabaseInterface
    {
        // section:manual_definitions
        // endsection:manual_definitions

      public:
        /**
         *
         * @param[in] filename
         * @return
         */
        virtual bool open(const std::string& filename) = 0;

        /**
         *
         */
        virtual void close() = 0;

      protected:
      private:
    };

} // namespace database

} // namespace aries

#endif /* DATABASE_INTERFACE_HPP */