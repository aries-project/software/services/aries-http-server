/**
 * aries-web-backend
 * Copyright (C)   Colibri-Embedded
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @file schema.hpp
 *
 * @author Daniel Kesler <kesler.daniel@gmail.com>
 *
 */
#ifndef SCHEMA_HPP
#define SCHEMA_HPP

#include "database/column_definition.hpp"
#include "database/database.hpp"
#include "database/schema_interface.hpp"
#include <string>
// section:manual_includes
// endsection:manual_includes

namespace aries {

namespace database {

    /**
     *
     */
    class Schema : public SchemaInterface
    {
        // section:manual_definitions
        // endsection:manual_definitions

      public:
        /**
         *
         * @param[in] database
         */
        Schema(Database& database);

        /**
         *
         * @param[in] table
         * @return
         */
        bool hasTable(const std::string& table) override;

        /**
         *
         * @param[in] table
         * @param[in] callback
         * @return
         */
        bool create(const std::string& table, const CreateTableCallback& callback) override;

        /**
         *
         * @param[in] table
         * @param[in] callback
         * @return
         */
        bool createIfNotExists(const std::string& table, const CreateTableCallback& callback) override;

        /**
         *
         * @param[in] table
         * @return
         */
        bool drop(const std::string& table) override;

        /**
         *
         * @param[in] table
         * @return
         */
        bool dropIfExists(const std::string& table) override;

        /**
         *
         * @param[in] column
         * @return
         */
        bool dropColumn(const std::string& column) override;

        /**
         *
         * @param[in] column
         * @return
         */
        bool renameColumn(const std::string& column) override;

        /**
         *
         * @param[in] column
         * @return
         */
        bool dropPrimary(const std::string& column) override;

        /**
         *
         * @param[in] column
         * @return
         */
        bool dropUnique(const std::string& column) override;

        /**
         *
         * @param[in] column
         * @return
         */
        bool dropIndex(const std::string& column) override;

        /**
         *
         * @param[in] column
         * @return
         */
        bool dropForeign(const std::string& column) override;

        /**
         * Set database value
         * @param[in] value
         */
        void setDatabase(const Database& value);

        /**
         * Get database value
         * @return
         */
        Database getDatabase() const;

      protected:
      private:
        ///
        Database m_database;
        /**
         *
         * @param[in] def
         * @return
         */
        std::string columnDefinitionToStatement(const ColumnDefinition& def) override;
    };

} // namespace database

} // namespace aries

#endif /* SCHEMA_HPP */