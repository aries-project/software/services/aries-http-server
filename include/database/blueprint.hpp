/**
 * aries-web-backend
 * Copyright (C)   Colibri-Embedded
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @file blueprint.hpp
 *
 * @author Daniel Kesler <kesler.daniel@gmail.com>
 *
 */
#ifndef BLUEPRINT_HPP
#define BLUEPRINT_HPP

#include "database/column_definition.hpp"
#include <string>
#include <vector>
// section:manual_includes
// endsection:manual_includes

namespace aries {

namespace database {

    /**
     *
     */
    class Blueprint
    {
        // section:manual_definitions
        // endsection:manual_definitions

      public:
        /**
         * The increments method creates an auto-incrementing UNSIGNED INTEGER equivalent column as a primary key.
         * @param[in] column
         * @return
         */
        ColumnDefinition& increments(const std::string& column);

        /**
         * The integer method creates an INTEGER equivalent column.
         * @param[in] column
         * @return
         */
        ColumnDefinition& integer(const std::string& column);

        /**
         * The float method creates a FLOAT equivalent column with the given precision (total digits) and scale (decimal
         * digits).
         * @param[in] column
         * @param[in] digits
         * @param[in] decimals
         * @return
         */
        ColumnDefinition& _float(const std::string& column, unsigned digits = 8, unsigned decimals = 2);

        /**
         * The double method creates a DOUBLE equivalent column with the given precision (total digits) and scale
         * (decimal digits).
         * @param[in] column
         * @param[in] digits
         * @param[in] decimals
         * @return
         */
        ColumnDefinition& _double(const std::string& column, unsigned digits = 8, unsigned decimals = 2);

        /**
         * The decimal method creates a DECIMAL equivalent column with the given precision (total digits) and scale
         * (decimal digits).
         * @param[in] column
         * @param[in] digits
         * @param[in] decimals
         * @return
         */
        ColumnDefinition& decimal(const std::string& column, unsigned digits = 8, unsigned decimals = 2);

        /**
         * The boolean method creates a BOOLEAN equivalent column.
         * @param[in] column
         * @return
         */
        ColumnDefinition& boolean(const std::string& column);

        /**
         * The string method creates a VARCHAR equivalent column of the given length.
         * @param[in] column
         * @param[in] length
         * @return
         */
        ColumnDefinition& string(const std::string& column, unsigned length = 0);

        /**
         * The text method creates a TEXT equivalent column.
         * @param[in] column
         * @return
         */
        ColumnDefinition& text(const std::string& column);

        /**
         * The enum method creates a ENUM equivalent column with the given valid values
         * @param[in] column
         * @param[in] values
         * @return
         */
        ColumnDefinition& _enum(const std::string& column, const std::vector<std::string>& values);

        /**
         * The timestamp method creates a TIMESTAMP equivalent column.
         * @param[in] column
         * @return
         */
        ColumnDefinition& timestamp(const std::string& column);

        /**
         * The timestamps method creates created_at and updated_at TIMESTAMP equivalent columns.
         */
        void timestamps();

        /**
         * The foreign method creates a FOREIGN KEY contraint.
         * @param[in] column
         * @return
         */
        ColumnDefinition& foreign(const std::string& column);

        /**
         * The foreignId method is an alias of the unsignedBigInteger method.
         * @param[in] column
         * @return
         */
        ColumnDefinition& foreignId(const std::string& column);

        /**
         * Get column definitions
         * @return
         */
        const std::vector<ColumnDefinition>& getColumns() const;

      protected:
      private:
        /// Column definitions
        std::vector<ColumnDefinition> m_columns;
    };

} // namespace database

} // namespace aries

#endif /* BLUEPRINT_HPP */