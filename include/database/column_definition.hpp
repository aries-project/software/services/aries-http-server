/**
 * aries-web-backend
 * Copyright (C)   Colibri-Embedded
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @file column_definition.hpp
 *
 * @author Daniel Kesler <kesler.daniel@gmail.com>
 *
 */
#ifndef COLUMN_DEFINITION_HPP
#define COLUMN_DEFINITION_HPP

#include <string>
#include <vector>
// section:manual_includes
// endsection:manual_includes

namespace aries {

namespace database {

    /**
     *
     */
    enum class ForeignKeyAction
    {
        NO_ACTION,
        RESTRICT,
        SET_NULL,
        SET_DEFAULT,
        CASCADE,
    };

    /**
     *
     */
    enum class CheckAction
    {
        IN_VALUES,
    };

    /**
     *
     */
    struct CheckExpression
    {
        ///
        CheckAction action;
        ///
        int value;
        ///
        std::vector<std::string> values;
    };

    /**
     *
     */
    class ColumnDefinition
    {
        // section:manual_definitions
        // endsection:manual_definitions

      public:
        /**
         *
         * @param[in] name
         * @param[in] _type
         * @param[in] size
         */
        ColumnDefinition(const std::string& name, const std::string& _type, const std::vector<unsigned>& size = {});

        /**
         *
         * @param[in] value
         * @return
         */
        ColumnDefinition& _default(const std::string& value);

        /**
         *
         * @param[in] value
         * @return
         */
        ColumnDefinition& _default(int value);

        /**
         *
         * @param[in] value
         * @return
         */
        ColumnDefinition& _default(double value);

        /**
         *
         * @param[in] value
         * @return
         */
        ColumnDefinition& _unsigned(bool value = true);

        /**
         *
         * @param[in] value
         * @return
         */
        ColumnDefinition& nullable(bool value = true);

        /**
         *
         * @param[in] value
         * @return
         */
        ColumnDefinition& unique(bool value = true);

        /**
         *
         * @param[in] value
         * @return
         */
        ColumnDefinition& index(bool value = true);

        /**
         *
         * @param[in] value
         * @return
         */
        ColumnDefinition& primary(bool value = true);

        /**
         *
         * @param[in] value
         * @return
         */
        ColumnDefinition& autoIncrement(bool value = true);

        /**
         *
         * @param[in] table
         * @return
         */
        ColumnDefinition& on(const std::string& table);

        /**
         *
         * @param[in] action
         * @return
         */
        ColumnDefinition& onDelete(ForeignKeyAction action);

        /**
         *
         * @param[in] action
         * @return
         */
        ColumnDefinition& onUpdate(ForeignKeyAction action);

        /**
         *
         * @param[in] column
         * @return
         */
        ColumnDefinition& references(const std::string& column);

        /**
         *
         * @param[in] action
         * @param[in] values
         * @return
         */
        ColumnDefinition& check(CheckAction action, const std::vector<std::string>& values);

        /**
         *
         * @param[in] action
         * @param[in] value
         * @return
         */
        ColumnDefinition& check(CheckAction action, int value);

        /**
         * Get name value
         * @return
         */
        std::string getName() const;

        /**
         * Get type value
         * @return
         */
        std::string getType() const;

        /**
         * Get size value
         * @return
         */
        const std::vector<unsigned>& getSize() const;

        /**
         * Get index value
         * @return
         */
        bool getIndex() const;

        /**
         * Get autoincrement value
         * @return
         */
        bool getAutoincrement() const;

        /**
         * Get nullable value
         * @return
         */
        bool getNullable() const;

        /**
         * Get primary value
         * @return
         */
        bool getPrimary() const;

        /**
         * Get unique value
         * @return
         */
        bool getUnique() const;

        /**
         * Get default value
         * @return
         */
        bool getDefault() const;

        /**
         * Get default_value value
         * @return
         */
        std::string getDefaultValue() const;

        /**
         * Get foreign value
         * @return
         */
        bool getForeign() const;

        /**
         * Get foreign_table value
         * @return
         */
        std::string getForeignTable() const;

        /**
         * Get foreign_references value
         * @return
         */
        std::string getForeignReferences() const;

        /**
         * Get on_delete value
         * @return
         */
        std::string getOnDelete() const;

        /**
         * Get on_update value
         * @return
         */
        std::string getOnUpdate() const;

        /**
         * Get checks value
         * @return
         */
        const std::vector<CheckExpression>& getChecks() const;

      protected:
      private:
        ///
        std::string m_name;
        ///
        std::string m_type;
        ///
        std::vector<unsigned> m_size = {};
        ///
        bool m_index = false;
        ///
        bool m_autoincrement = false;
        ///
        bool m_nullable = false;
        ///
        bool m_primary = false;
        ///
        bool m_unique = false;
        ///
        bool m_default = false;
        ///
        std::string m_default_value;
        ///
        bool m_foreign = false;
        ///
        std::string m_foreign_table;
        ///
        std::string m_foreign_references;
        ///
        std::string m_on_delete;
        ///
        std::string m_on_update;
        ///
        std::vector<CheckExpression> m_checks;
    };

} // namespace database

} // namespace aries

#endif /* COLUMN_DEFINITION_HPP */