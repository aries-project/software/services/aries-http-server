/**
 * aries-web-backend
 * Copyright (C)   Colibri-Embedded
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @file user.hpp
 *
 * @author Daniel Kesler <kesler.daniel@gmail.com>
 *
 */
#ifndef USER_HPP
#define USER_HPP

#include <string>
// section:manual_includes
// endsection:manual_includes

namespace aries {

namespace database {

    /**
     *
     */
    class User
    {
        // section:manual_definitions
        // endsection:manual_definitions

      public:
        /**
         * Set id value
         * @param[in] value
         */
        void setId(int value);

        /**
         * Get id value
         * @return
         */
        int getId() const;

        /**
         * Set name value
         * @param[in] value
         */
        void setName(const std::string& value);

        /**
         * Get name value
         * @return
         */
        std::string getName() const;

        /**
         * Set email value
         * @param[in] value
         */
        void setEmail(const std::string& value);

        /**
         * Get email value
         * @return
         */
        std::string getEmail() const;

        /**
         * Set password value
         * @param[in] value
         */
        void setPassword(const std::string& value);

        /**
         * Get password value
         * @return
         */
        std::string getPassword() const;

        /**
         * Set avatar_url value
         * @param[in] value
         */
        void setAvatarUrl(const std::string& value);

        /**
         * Get avatar_url value
         * @return
         */
        std::string getAvatarUrl() const;

        /**
         * Set role value
         * @param[in] value
         */
        void setRole(unsigned value);

        /**
         * Get role value
         * @return
         */
        unsigned getRole() const;

      protected:
      private:
        ///
        int m_id;
        ///
        std::string m_name;
        ///
        std::string m_email;
        ///
        std::string m_password;
        ///
        std::string m_avatar_url;
        ///
        unsigned m_role;
    };

} // namespace database

} // namespace aries

#endif /* USER_HPP */