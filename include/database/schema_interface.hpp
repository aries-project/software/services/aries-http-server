/**
 * aries-web-backend
 * Copyright (C)   Colibri-Embedded
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @file schema_interface.hpp
 *
 * @author Daniel Kesler <kesler.daniel@gmail.com>
 *
 */
#ifndef SCHEMA_INTERFACE_HPP
#define SCHEMA_INTERFACE_HPP

#include "database/blueprint.hpp"
#include "database/column_definition.hpp"
#include <functional>
#include <string>
// section:manual_includes
// endsection:manual_includes

namespace aries {

namespace database {

    typedef std::function<void(aries::database::Blueprint& table)> CreateTableCallback;

    /**
     *
     */
    class SchemaInterface
    {
        // section:manual_definitions
        // endsection:manual_definitions

      public:
        /**
         *
         * @param[in] table
         * @return
         */
        virtual bool hasTable(const std::string& table) = 0;

        /**
         *
         * @param[in] table
         * @param[in] callback
         * @return
         */
        virtual bool create(const std::string& table, const CreateTableCallback& callback) = 0;

        /**
         *
         * @param[in] table
         * @param[in] callback
         * @return
         */
        virtual bool createIfNotExists(const std::string& table, const CreateTableCallback& callback) = 0;

        /**
         *
         * @param[in] table
         * @return
         */
        virtual bool drop(const std::string& table) = 0;

        /**
         *
         * @param[in] table
         * @return
         */
        virtual bool dropIfExists(const std::string& table) = 0;

        /**
         *
         * @param[in] column
         * @return
         */
        virtual bool dropColumn(const std::string& column) = 0;

        /**
         *
         * @param[in] column
         * @return
         */
        virtual bool renameColumn(const std::string& column) = 0;

        /**
         *
         * @param[in] column
         * @return
         */
        virtual bool dropPrimary(const std::string& column) = 0;

        /**
         *
         * @param[in] column
         * @return
         */
        virtual bool dropUnique(const std::string& column) = 0;

        /**
         *
         * @param[in] column
         * @return
         */
        virtual bool dropIndex(const std::string& column) = 0;

        /**
         *
         * @param[in] column
         * @return
         */
        virtual bool dropForeign(const std::string& column) = 0;

      protected:
      private:
        /**
         *
         * @param[in] def
         * @return
         */
        virtual std::string columnDefinitionToStatement(const ColumnDefinition& def) = 0;
    };

} // namespace database

} // namespace aries

#endif /* SCHEMA_INTERFACE_HPP */