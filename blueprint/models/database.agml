interface DatabaseInterface
    @namespace aries.database
    @package.path "database"

    method open(filename: String): Bool
    method close()

model Database implements DatabaseInterface
    @namespace aries.database
    @package.path "database"

interface SchemaInterface
    @namespace aries.database
    @package.path "database"

    method has_table(table: String): Bool
    method create(table: String, callback: createTableCallback): Bool
    method create_if_not_exists(table: String, callback: createTableCallback): Bool
    method drop(table: String): Bool
    method drop_if_exists(table: String): Bool

    method drop_column(column: String): Bool
    method rename_column(column: String): Bool
    method drop_primary(column: String): Bool
    method drop_unique(column: String): Bool
    method drop_index(column: String): Bool
    method drop_foreign(column: String): Bool

    method column_definition_to_statement(def: ColumnDefinition): String
        @private

enum ForeignKeyAction
    @namespace aries.database
    @package "column_definition"
    @package.path "database"

    NO_ACTION
    RESTRICT
    SET_NULL
    SET_DEFAULT
    CASCADE

enum CheckAction
    @namespace aries.database
    @package "column_definition"
    @package.path "database"

    IN_VALUES

struct CheckExpression
    @namespace aries.database
    @package "column_definition"
    @package.path "database"

    attr action: CheckAction
    attr value: Int
    attr values: [String]

model ColumnDefinition
    @namespace aries.database
    @package.path "database"

    method __create__(name: String, type: String, size: [Unsigned] = {})

    method default(value: String): @ref ColumnDefinition
    method default(value: Int): @ref ColumnDefinition
    method default(value: Double): @ref ColumnDefinition

    method unsigned(value: Bool = True): @ref ColumnDefinition
    method nullable(value: Bool = True): @ref ColumnDefinition
    method unique(value: Bool = True): @ref ColumnDefinition
    method index(value: Bool = True): @ref ColumnDefinition
    method primary(value: Bool = True): @ref ColumnDefinition
    method auto_increment(value: Bool = True): @ref ColumnDefinition

    method on(table: String): @ref ColumnDefinition
    method on_delete(action: ForeignKeyAction): @ref ColumnDefinition
    method on_update(action: ForeignKeyAction): @ref ColumnDefinition
    method references(column: String): @ref ColumnDefinition

    method check(action: CheckAction, values: [String]): @ref ColumnDefinition
    method check(action: CheckAction, value: Int): @ref ColumnDefinition

    attr name: String
        @readonly

    attr type: String
        @readonly

    attr size: [Unsigned] = {}
        @readonly

    attr index: Bool = False
        @readonly

    attr autoincrement: Bool = False
        @readonly

    attr nullable: Bool = False
        @readonly

    attr primary: Bool = False
        @readonly

    attr unique: Bool = False
        @readonly

    attr default: Bool = False
        @readonly

    attr default_value: String
        @readonly

    attr foreign: Bool = False
        @readonly

    attr foreign_table: String
        @readonly

    attr foreign_references: String
        @readonly

    attr on_delete: String
        @readonly

    attr on_update: String
        @readonly

    attr checks: [CheckExpression]
        @readonly

model Blueprint
    @namespace aries.database
    @package.path "database"

    ### Methods

    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    The increments method creates an auto-incrementing UNSIGNED INTEGER equivalent column as a primary key.
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    method increments(column: String): @ref ColumnDefinition 
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    The integer method creates an INTEGER equivalent column.
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    method integer(column: String): @ref ColumnDefinition
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    The float method creates a FLOAT equivalent column with the given precision (total digits) and scale (decimal digits).
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    method float(column: String, digits: Unsigned = 8, decimals: Unsigned = 2): @ref ColumnDefinition
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    The double method creates a DOUBLE equivalent column with the given precision (total digits) and scale (decimal digits).
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    method double(column: String, digits: Unsigned = 8, decimals: Unsigned = 2): @ref ColumnDefinition
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    The decimal method creates a DECIMAL equivalent column with the given precision (total digits) and scale (decimal digits).
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    method decimal(column: String, digits: Unsigned = 8, decimals: Unsigned = 2): @ref ColumnDefinition
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    The boolean method creates a BOOLEAN equivalent column.
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    method boolean(column: String): @ref ColumnDefinition
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    The string method creates a VARCHAR equivalent column of the given length.
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    method string(column: String, length: Unsigned = 0): @ref ColumnDefinition
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    The text method creates a TEXT equivalent column.
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    method text(column: String): @ref ColumnDefinition
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    The enum method creates a ENUM equivalent column with the given valid values
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    method _enum(column: String, values: [String]): @ref ColumnDefinition
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    The timestamp method creates a TIMESTAMP equivalent column.
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    method timestamp(column: String): @ref ColumnDefinition
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    The timestamps method creates created_at and updated_at TIMESTAMP equivalent columns.
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    method timestamps()

    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    The foreign method creates a FOREIGN KEY contraint.
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    method foreign(column: String): @ref ColumnDefinition

    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    The foreignId method is an alias of the unsignedBigInteger method.
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    method foreignId(column: String): @ref ColumnDefinition

    ### Attributes

    attr columns: [ColumnDefinition] # Column definitions
        @readonly

delegate createTableCallback -> (table: Blueprint)
    @namespace aries.database
    @package schema_interface
    @package.path "database"

model Schema implements SchemaInterface
    @namespace aries.database
    @package.path "database"

    method __create__
        @init.attrs [database]

    attr database: Database

interface MigrationInterface
    @namespace aries.database
    @package.path "database"

    ~~~~~~~~~~~~~~~~~~
    Run the migration.
    ~~~~~~~~~~~~~~~~~~
    method up -> (schema: SchemaInterface)

    ~~~~~~~~~~~~~~~~~~~~~~
    Reverse the migration.
    ~~~~~~~~~~~~~~~~~~~~~~
    method down -> (schema: SchemaInterface)

prototype DatabaseModels basedon "agml/database_models.agml.template"
    @namespace aries.database
    @prototype.output_name "database_models.agml"